package coinpurse;
import java.util.Arrays;
import java.util.List;

/**
 * Utilities for Lists.
 * @author Pitchaya Namchaisiri
 *
 */
public class ListUtil {
	/**
	 * Print all items in the given list.
	 * @param list is the list to print out.
	 */
	private static void printList(List<?> list) {
		System.out.print(printListTo(list, list.size() - 1));

	}
	private static String printListTo(List<?> list, int index) {
		String temp = "";
		if (index <= 0) return list.get(0).toString();
		temp = printListTo(list, index - 1) + ", " + list.get(index);
		return temp;
	}
	
	/**
	 * Find the maximum String in the list.
	 * @param list is the list to find maximum.
	 * @return the max String.
	 */
	private static String max(List<String> list) {
		return maxTo(list,list.size() - 1);
	}
	private static String maxTo(List<String> list, int index) {
		String temp = "";
		if (index <= 0) return list.get(0);
		if (list.get(index).compareTo(maxTo(list, index - 1)) > 0) {
			temp = list.get(index);
		}
		else temp = maxTo(list, index - 1);
		
		return temp;
		
	}
	/**
	 * For testing code. 
	 * @param args code for testing.
	 */
	public static void main(String[] args) {
		List<String> x;
		x = Arrays.asList("A","B","C","D");
		printList(x);
		System.out.println();
		List<String> list;
		if (args.length > 0) list = Arrays.asList(args);
		else list = Arrays.asList("bird","zebra","cat","pig");
		
		System.out.println("List contains: ");
		printList(list);
		System.out.println();
		String max = max(list);
		System.out.println("Lexically greatest element is " + max);
	}
}
