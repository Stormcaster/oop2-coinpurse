package coinpurse;
/**
 * Abstract superclass for Valuables.
 * @author Pitchaya Namchaisiri
 *
 */
public abstract class AbstractValuable implements Valuable {
	
	//value of any valuables.
	/**
	 * Constructor.
	 */
	public AbstractValuable() {
	}

	/**
	 * @param other is any other object
	 * @return true if other is Coin and has the same value as this Coin. Otherwise false.
	 */
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other.getClass() != this.getClass()) return false;
		Valuable another = (Valuable) other;
		if (another.getValue() == this.getValue()) return true;
		return false;
	}
	/**
	 * @param other is valuable to compare with this valuable.
	 * @return 1 if this valuable has more value than other valuable. -1 if vice versa. 0 if equal.
	 */
	public int compareTo (Valuable other) {
		if (this.getValue() > other.getValue()) return 1;
		else if (this.getValue() < other.getValue()) return -1;
		
		return 0;
	}

}
