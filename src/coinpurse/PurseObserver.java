package coinpurse;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Observer for the purse.
 * @author Pitchaya Namchaisiri
 *
 */
public class PurseObserver implements Observer{
	/**
	 * Constructor. No reference to Purse needed.
	 */
	public PurseObserver() {
		
	}
	/**
	 * Receives notification from the purse.
	 * @param subject is the object observed.
	 * @param info is information of the object.
	 */
	public void update(Observable subject, Object info) {
		if (subject instanceof Purse) {
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			System.out.println("Balance is: " + balance);
		}
		if (info != null) System.out.println(info);
	}
	
}



