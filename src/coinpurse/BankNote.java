package coinpurse;
/**
 * A banknote with monetary value.
 * @author Pitchaya Namchaisiri
 *
 */
public class BankNote extends AbstractValuable {
	/** Value of this banknote. */
	private double value;
	private static long nextSerialNumber = 1000000;
	private long serialNumber;
	private String currency;
	/**
	 * Constructor for a new banknote.
	 * @param value is value of the new banknote.
	 */
	public BankNote(double value, String currency) {	
		//		if (value == 20 || value == 50 || value == 100 || value == 500 || value == 1000) {
		this.value = value;
		this.currency = currency;
		this.serialNumber = getNextSerialNumber();
		//		}
		//		else value = 0;

	}
	/**
	 * 
	 * @return the next unique serial number of the banknote.
	 */
	private long getNextSerialNumber() {
		nextSerialNumber++;
		return nextSerialNumber;
	}

	/**
	 * @return value of this banknote.
	 */
	public double getValue() {
		return value;
	}
	/**
	 * @return String description of the banknote.
	 */
	public String toString() {
		return this.value + "-" + this.currency + " Banknote " + serialNumber;
	}

}
