package coinpurse;



/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Pitchaya Namchaisiri
 */
public class Coin extends AbstractValuable {

	/** Value of the coin. */
	private double value;
	private String currency;
	
	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value, String currency ) {
		this.value = value;
		this.currency = currency;
	}

	/**
	 * 
	 * @return the value of this coin
	 */
	public double getValue() {
		return this.value;
	}
	/**
	 * @return String value of this coin.
	 */
	public String toString() {
		return this.value + "-" + this.currency + " coin";
	}


}
