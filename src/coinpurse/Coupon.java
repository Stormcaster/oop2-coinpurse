package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * A coupon with different colors.
 * @author Pitchaya Namchaisiri
 *
 */
public class Coupon extends AbstractValuable {
	/** Map for values of a coupon by color. */
	private Map<String , Double> valueMap = new HashMap<String,Double>( );
	/** Color of the coupon. */
	private String color;
	/**
	 * Initialize the map and transform input color to uppercase.
	 * @param color is the color of the new coupon
	 */
	public Coupon(String color) {
		valueMap.put("red", 100.0);
		valueMap.put("blue", 50.0);
		valueMap.put("green", 20.0);
		if (valueMap.containsKey(color.toLowerCase())) {
			
			this.color = color.toLowerCase();
		}
		else color = null;
	}
	/**
	 * @return value of the coupon.
	 */
	public double getValue() {
		return (double)(this.valueMap.get(this.color));
	}
	/**
	 * @return the String description of the coupon.
	 */
	public String toString() {
		return this.color + " coupon";
	}
	

}
