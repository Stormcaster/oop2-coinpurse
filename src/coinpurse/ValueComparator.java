package coinpurse;

import java.util.Comparator;
/**
 * Comparator for valuables.
 * @author Pitchaya Namchaisiri
 *
 */
public class ValueComparator implements Comparator<Valuable> {

	/**
	 * Compare the value of each valuables.
	 * @param val1 is the first valuable. 
	 * @param val2 is the second valuable.
	 * @return -1 if val1 has less value than val2,
	 * 1 if val1 has more value than val2,
	 * 0 if a and b have same value.
	 */
	public int compare(Valuable val1, Valuable val2) {
		if (val1.getValue() > val2.getValue()) return -1;
		else if (val1.getValue() == val2.getValue()) {
			return 0;
		}
		else return 1;
	}

}
