package coinpurse;

import java.util.Observer;

import coinpurse.strategy.RecursiveWithdraw;

 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Pitchaya Namchaisiri
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
        // 1. create a Purse
    	Purse purse = new Purse(10);
    	purse.setWithdrawStrategy(new RecursiveWithdraw());
    	purse.addObserver(new PurseObserver());
    	PurseBalanceObserver balObs = new PurseBalanceObserver();
    	purse.addObserver(balObs);
    	PurseStatusObserver statObs = new PurseStatusObserver();
    	purse.addObserver(statObs);
    	
    	
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog consoleD = new ConsoleDialog(purse);

//        // 3. run() the ConsoleDialog
    	balObs.run();
    	statObs.run();
    	consoleD.run();
    	
    	//GreedyWithdraw fails when purse contains 5,2,2,2 but RecursiveWithdraw works.

    }
}
