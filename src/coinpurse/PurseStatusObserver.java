package coinpurse;

import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * Dialog for observing purse status.
 * @author Pitchaya Namchaisiri
 *
 */
public class PurseStatusObserver extends JFrame implements Runnable, Observer{
	JLabel statLbl;
	JProgressBar statProgBar;
	/**
	 * Constructor for the dialog.
	 */
	public PurseStatusObserver() {
		this.initComponents();
	}
	/**
	 * Components for the GUI.
	 */
	private void initComponents() {
		super.setTitle("Purse Status");
		super.setSize(400, 100);
		super.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		statLbl = new JLabel("EMPTY");
		statLbl.setFont(new Font("Calibri",Font.BOLD,26));
		statProgBar = new JProgressBar();
		statProgBar.setMinimum(0);
		statProgBar.setPreferredSize(new Dimension(400,75));
		super.add(statLbl);
		super.add(statProgBar);


	}
	@Override
	/**
	 * Receives update from Observer.
	 */
	public void update(Observable subject, Object arg) {
		if (subject instanceof Purse) {
			Purse purse = (Purse)subject;
			statProgBar.setMaximum(purse.getCapacity());
			statProgBar.setValue(purse.count());
			if (purse.getCapacity() == purse.count()) {
				statLbl.setText("FULL");
			}
			else if (purse.count() == 0) statLbl.setText("EMPTY");
			else {
				statLbl.setText(String.format("%d of %d full", purse.count(), purse.getCapacity()));
			}
		}		
	}
	@Override
	/**
	 * Runs the GUI.
	 */
	public void run() {
		this.setVisible(true);
	}
}
