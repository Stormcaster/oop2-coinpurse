package coinpurse;

import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

/**
 * Dialog for showing purse balance.
 * @author Pitchaya Namchaisiri
 *
 */
public class PurseBalanceObserver extends JFrame implements Observer,Runnable{
	JLabel balanceLbl;
	
	/**
	 * Constructor of the UI.
	 */
	public PurseBalanceObserver() {
		this.initComponents();
	}
	/**
	 * Components for the GUI.
	 */
	private void initComponents() {
		super.setTitle("Purse Balance");
		super.setSize(250, 90);
		super.setLayout(new FlowLayout());
		balanceLbl = new JLabel("0.00");
		balanceLbl.setFont(new Font("Calibri",Font.BOLD,26));

		super.add(balanceLbl);
	}
	@Override
	/**
	 * Runs the GUI.
	 */
	public void run() {
		this.setVisible(true);
	}
	@Override
	/**
	 * Receives updates from Observer.
	 */
	public void update(Observable subject, Object arg) {
		if (subject instanceof Purse) {
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			balanceLbl.setText(String.format("%.2f Baht", balance));
		}
	}
}
