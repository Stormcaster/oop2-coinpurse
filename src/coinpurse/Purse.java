package coinpurse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the valuable purse decides which
 *  valuables to remove.
 *  
 *  @author Pitchaya Namchaisiri
 */
public class Purse extends Observable{
	/** Collection of valuables in the purse. */
	private List<Valuable> valuableList;
	private WithdrawStrategy strategy = new GreedyWithdraw();
	private final ValueComparator comparator = new ValueComparator();

	/** Capacity is maximum NUMBER of valuables the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of valuables you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		valuableList = new ArrayList<Valuable>();
	}
	/**
	 * Sets the withdrawing strategy of this purse.
	 * @param strategy is the strategy to use.
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy) {
		this.strategy = strategy;
	}

	/**
	 * Count and return the number of valuables in the purse.
	 * This is the number of valuables, not their value.
	 * @return the number of valuables in the purse
	 */
	public int count() {
		if (valuableList != null) return valuableList.size();
		else return 0;
	}
	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		if (valuableList != null) {
			double total = 0;
			for (int i = 0; i < valuableList.size(); i++) {
				total += valuableList.get(i).getValue();
			}
			return total;
		}
		return 0;
	}


	/**
	 * Return the capacity of the money purse.
	 * @return the capacity
	 */
	public int getCapacity() { return this.capacity; }

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if (this.capacity == this.count()) return true;
		return false;

	}

	/** 
	 * Insert a valuable into the purse.
	 * The valuable is only inserted if the purse has space for it
	 * and the valuable has positive value.  No worthless valuables!
	 * @param valuable is a Valuable object to insert into purse
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert( Valuable valuable ) {
		// if the purse is already full then can't insert anything.
		if (valuable.getValue() <= 0) return false;
		if (isFull()) return false;
		valuableList.add(valuable);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Valuables withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		/*
		 * One solution is to start from the most valuable thing
		 * in the purse and take any valuable that maybe used for
		 * withdraw.
		 * Since you don't know if withdraw is going to succeed, 
		 * don't actually withdraw the valuables from the purse yet.
		 * Instead, create a temporary list.
		 * Each time you see a valuable that you want to withdraw,
		 * add it to the temporary list and deduct the value
		 * from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter,
		 * use a local total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), 
		 * then you are done.
		 * Now you can withdraw the valuables from the purse.
		 * NOTE: Don't use list.removeAll(templist) for this
		 * becuase removeAll removes *all* valuables from list that
		 * are equal (using Valuable.equals) to something in templist.
		 * Instead, use a loop over templist
		 * and remove valuables one-by-one.		
		 */
		if (amount <= 0 || amount > this.getBalance()) return null;
		Collections.sort(valuableList, comparator);
		Valuable[] remove = strategy.withdraw(amount, valuableList);

		if (remove != null) {
			for (int i = 0; i < remove.length; i++) {
				valuableList.remove(remove[i]);
			}
		}
		super.setChanged();
		super.notifyObservers(this);
		return remove;

		//		return strat.withdraw(amount, this.valuableList);
	}

	/** 
	 * @return a string description of the purse contents.
	 */
	public String toString() {
		String text = "";
		text += "This purse contains....\n";
		for (int i = 0; i < valuableList.size(); i++) {
			text += valuableList.get(i).toString() + "\n";
		}
		return text;
	}

}