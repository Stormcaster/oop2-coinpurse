package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * Strategy for withdrawal from a purse.
 * @author Pitchaya Namchaisiri
 *
 */
public interface WithdrawStrategy {
	/**  
	 *  Suggest the valuables to withdraw according to the requested amount of money.
	 *  @param amount is the amount to withdraw
	 *  @param valuables is the array of valuables to withdraw from
	 *  @return array of Valuable objects for money withdrawn, 
	 *  		or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables);

}
