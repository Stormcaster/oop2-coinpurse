package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * Withdraw using recursive strategy.
 * @author Pitchaya Namchaisiri
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		Collections.sort(valuables, new ValueComparator());
		Collections.reverse(valuables);

		List<Valuable> rmv = withdrawFrom(amount,valuables,valuables.size() - 1);
		
		if (rmv != null) {
			Valuable[] rmvArr = new Valuable[rmv.size()];
			for (int i = 0; i < rmv.size(); i++) {
				rmvArr[i] = rmv.get(i);
			}
			return rmvArr;
		}
		else {
			return null;
		}
	}
	private List<Valuable> withdrawFrom(double amount, List<Valuable> list, int index) {
		if (index < 0) {
			return null;
		}
		if (amount < 0) {
			return null;
		}
		
		Valuable pick = list.get(index);
		if (pick.getValue() - amount == 0) {
			List<Valuable> rtnTemp = new ArrayList<Valuable>();
			rtnTemp.add(pick);
			return rtnTemp;
		}
		List<Valuable> temp = withdrawFrom(amount - pick.getValue(),list,index - 1);
		if (temp == null) {
			temp = withdrawFrom(amount,list,index - 1);
		}
		else {
			temp.add(pick);
		}
		return temp;


	}

}