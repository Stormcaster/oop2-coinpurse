package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * Use the greedy method to withdraw.
 * @author Pitchaya Mamchaisiri
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{

	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> tmp = new ArrayList<Valuable>();
		Collections.sort(valuables, new ValueComparator());
		double tempTotal = amount;
		for (int i = 0; i < valuables.size(); i++) {
			Valuable thisVal = valuables.get(i);
			if (tempTotal - thisVal.getValue() >= 0) {
				tmp.add(thisVal);
				tempTotal -= thisVal.getValue();
			}
		}

		if (tempTotal == 0) {
			Valuable[] rmvArr = new Valuable[tmp.size()];
			for (int i = 0; i < rmvArr.length; i++) rmvArr[i] = tmp.get(i);
			return rmvArr;
		}
		else return null;

	}

}
