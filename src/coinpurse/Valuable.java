package coinpurse;
/**
 * Interface for all kinds of money that are valuable. 
 * @author Pitchaya Namchaisiri
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * This gets the value of the kind of money specified.
	 * @return value of given valuable.
	 */
	public double getValue();
}

