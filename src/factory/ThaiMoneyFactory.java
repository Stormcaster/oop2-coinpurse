package factory;

import java.util.Arrays;
import java.util.List;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;

public class ThaiMoneyFactory extends MoneyFactory {
private String currencyName = "Baht";
	@Override
	public Valuable createMoney(double value) {
		List<Double> coinVal = Arrays.asList(1.0,2.0,5.0,10.0);
		List<Double> bankVal = Arrays.asList(20.0,50.0,100.0,500.0,1000.0);
		if (coinVal.contains(value)) {
			return new Coin(value, currencyName);
		}
		else if (bankVal.contains(value)) {
			return new BankNote(value, currencyName);
		}
		else throw new IllegalArgumentException();
	}
}
