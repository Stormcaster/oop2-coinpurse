package factory;

import java.util.ResourceBundle;

import coinpurse.Valuable;
/**
 * Factory singleton for creating Valuables. 
 * @author Pitchaya Namchaisiri
 *
 */
public abstract class MoneyFactory {
	/**
	 * The instance of the factory.
	 */
	private static MoneyFactory instance;
	/**
	 * Empty constructor.
	 */
	protected MoneyFactory() {}
	/**
	 * Gets the instance of this factory. If it is not instantiated yet, create it according to the purse.properties file.
	 * @return Instance of this factory.
	 */
	public static synchronized MoneyFactory getInstance() {
		if (instance == null) initFactory();
		return instance;
	}
	/**
	 * Sets the instance to be a preferred factory.
	 */
	static void setMoneyFactory() {
		if (instance == null) instance = new MalaysianMoneyFactory();
	}
	/**
	 * Create a coin or a purse of given value. Must be according to real currency.
	 * @param value is the value to create money from.
	 * @return Valuable created.
	 */
	public abstract Valuable createMoney(double value);
	Valuable createMoney(String value) {
		return createMoney(Double.parseDouble(value));
	}
	/**
	 * Initializes factory according to purse.properties file.
	 */
	private static void initFactory() {
		ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
		String factoryclass = bundle.getString("moneyfactory");
		//TODO if factoryclass is null then use a default class
		//this is for testing...
		System.out.println("Factory class is " + factoryclass); // testing
		try {
			instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			System.out.println("Error creating MoneyFactory "+e.toString() );
			instance = new ThaiMoneyFactory();
		}
		
	}

}