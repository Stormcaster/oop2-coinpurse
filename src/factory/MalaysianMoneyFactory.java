package factory;

import java.util.Arrays;
import java.util.List;

import coinpurse.BankNote;
import coinpurse.Coin;
import coinpurse.Valuable;

public class MalaysianMoneyFactory extends MoneyFactory {
private String currencyName = "Ringgit";
private String subCurrencyName = "Sen";
	@Override
	public Valuable createMoney(double value) {
		List<Double> coinVal = Arrays.asList(0.05,0.1,0.2,0.5);
		List<Double> bankVal = Arrays.asList(1.0,2.0,5.0,10.0,20.0,50.0,100.0);
		if (coinVal.contains(value)) {
			return new Coin(value, subCurrencyName);
		}
		else if (bankVal.contains(value)) {
			return new BankNote(value, currencyName);
		}
		else throw new IllegalArgumentException();

	}
	
}
